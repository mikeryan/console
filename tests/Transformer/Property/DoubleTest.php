<?php

namespace Soong\Console\Tests\Transformer\Property;

use Soong\Tests\Contracts\Transformer\PropertyTransformerTestBase;

/**
 * Tests the \Soong\Transformer\Property\Double class.
 */
class DoubleTest extends PropertyTransformerTestBase
{

    /**
     * Specify the class we're testing.
     */
    protected function setUp() : void
    {
        $this->transformerClass = '\Soong\Console\Transformer\Property\Double';
    }

    /**
     * Test doubling of various types of values
     *
     * @return array
     */
    public function transformerDataProvider() : array
    {
        return [
            'positive integer' => [[], 1, 2, null],
            'negative integer' => [[], -1, -2, null],
            'numeric string' => [[], '1', 2, null],
            'zero' => [[], 0, 0, null],
            'float' => [[], 1.2345, 2.469, null],
            'null' => [[], null, null, null],
            'boolean' => [[], true, null,
                'Double property transformer: expected numeric value, received boolean'],
            'non-numeric string' => [[], 'Blah', null,
                'Double property transformer: expected numeric value, received string'],
            'array' => [[], ['Blah'], null,
                'Double property transformer: expected numeric value, received array'],
            'object' => [[], new \stdClass, null,
                'Double property transformer: expected numeric value, received object'],
        ];
    }
}
