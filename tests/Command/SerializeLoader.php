<?php
declare(strict_types=1);

namespace Soong\Console\Tests\Command;

use Soong\Contracts\Data\RecordPayload;
use Soong\Loader\LoaderBase;

/**
 * Simple loader for testing with persistence.
 */
class SerializeLoader extends LoaderBase
{

    use SerializeTrait;

    /**
     * @inheritdoc
     */
    protected function optionDefinitions(): array
    {
        $options = parent::optionDefinitions();
        $options['file'] = [
            'required' => true,
            'allowed_types' => 'string',
        ];
        return $options;
    }

    /**
     * @inheritdoc
     */
    public function __invoke(RecordPayload $payload) : RecordPayload
    {
        $completeData = $this->getData();
        $completeData[] = $payload->getDestinationRecord();
        $this->putData($completeData);
        return $payload;
    }

    /**
     * @inheritdoc
     */
    public function delete(array $loadedKey) : void
    {
        $completeData = $this->getData();
        /** @var \Soong\Contracts\Data\Record $data */
        foreach ($completeData as $index => $data) {
            if ($loadedKey == [$data->getPropertyValue('id')]) {
                unset($completeData[$index]);
            }
        }
        $this->putData($completeData);
    }
}
