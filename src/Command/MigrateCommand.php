<?php
declare(strict_types=1);

namespace Soong\Console\Command;

use League\Pipeline\Pipeline;
use Soong\Task\MigrateOperation;
use Soong\Task\TaskPayload;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Implementation of the "migrate" console command.
 */
class MigrateCommand extends EtlCommand
{

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName("migrate")
            ->setDescription("Migrate data from one place to another")
            ->setDefinition([
                $this->tasksArgument(),
                $this->directoryOption(),
                $this->selectOption(),
                $this->limitOption(),
            ])
            ->setHelp(<<<EOT
The <info>migrate</info> command executes a Soong migration task
EOT
          );
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var string[] $directoryNames */
        $directoryNames = $input->getOption('directory');
        $options = [
            'select' => $input->getOption('select'),
            'limit' => $input->getOption('limit'),
        ];
        $taskContainer = $this->loadConfiguration($directoryNames, $options);
        $pipeline = new Pipeline();
        foreach ($input->getArgument('tasks') as $id) {
            if ($task = $taskContainer->get($id)) {
                $output->writeln("<info>Executing $id</info>");
                $pipeline = $pipeline->pipe(new MigrateOperation($task));
            } else {
                $output->writeln("<error>Task $id not found</error>");
            }
        }
        $payload = new TaskPayload($options);
        $pipeline->process($payload);
    }
}
